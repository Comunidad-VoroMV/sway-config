#!/bin/bash

killall swayidle

swayidle -w \
	timeout 120 'bash /home/rober/.config/sway/swaylock.sh' \
	timeout 300 'systemctl suspend' \
	before-sleep 'bash /home/rober/.config/sway/swaylock.sh'
