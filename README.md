# Sway Config

> Configuración propia del windows manager 100% Wayland Sway con los colores de Dracula Theme

![Screenshot](./screenshot/escritorio.png)

## Como instalarlo

La mayor parte de los ficheros están en la carpeta personal, dentro de la oculta .config (/home/<usuario>/.config)

Descarga y copialos en las mismas carpetas

Los fondos de escritorios están en la carpeta wallpaper Dracula y son copiados directamente de este proyecto
En su interior se encuentra el fichero README.md y la licencia (MIT)

El tema de Firefox lo encontraréis aquí:

https://addons.mozilla.org/en-US/firefox/addon/arch-dracula/

https://addons.mozilla.org/en-US/firefox/addon/arch-dracula-dark/

## Algunas capturas

![Screenshot](./screenshot/escritorio-terminal.png)

![Screenshot](./screenshot/escritorio-mako2.png)

![Screenshot](./screenshot/escritorio-wofi.png)

![Screenshot](./screenshot/escritorio-firefox.png)
